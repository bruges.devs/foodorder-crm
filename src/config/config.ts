import {LogLevel} from "../core/shared/constants/loglevel";

export class Config {

  public static SERVER_APP_URL = "https://calm-dusk-20347.herokuapp.com/api";

  public static LOG_LEVELS: Array<LogLevel> = [LogLevel.FINE, LogLevel.DEBUG, LogLevel.INFO, LogLevel.WARN, LogLevel.ERROR];

  public static IS_IN_DEBUG = true;

  public static TOAST_DURATION_NORMAL: number = 2000;

}
