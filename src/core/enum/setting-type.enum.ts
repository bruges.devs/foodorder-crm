export enum SettingTypeEnum {
  BOOLEAN,
  NUMBER,
  TEXT,
  LIST
}
