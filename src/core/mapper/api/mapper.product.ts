import {RowMapper} from "../../shared/mapper/rowmapper";
import {Product} from "../../../providers/index";
import {MapperCategory} from "./mapper.category";

export class MapperProduct implements RowMapper<Product>{
  mapRow(from: any, index: number): Product {
    let product: Product = {};
    product.id = from.product_id;
    product.name = from.name;
    product.description = from.description ? from.description: '';
    product.price = from.price;
    product.category = new MapperCategory().mapRow(from, index);
    return product;
  }

}
