import {Category} from "../../../providers";
import {RowMapper} from "../../shared/mapper/rowmapper";
import {CategoryModel} from "../../model/category.model";

export class MapperCategory implements RowMapper<Category>{
  mapRow(from: any, index: number): CategoryModel {
   let category = new CategoryModel();
   category.id = from.id;
   category.name = from.name;
   return category;
  }

  mapList(from: any, index: number): CategoryModel[] {
    let categories = [];
    for (let i = 0; i < from.length; i++) {
      categories.push(this.mapRow(from[i], index));
    }
    return categories;
  }

}
