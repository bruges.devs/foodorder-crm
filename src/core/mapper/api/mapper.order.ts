import {OrderModel} from "../../model/order.model";
import {RowMapper} from "../../shared/mapper/rowmapper";
import {MapperOrderItem} from "./mapper.order-item";
import {OrderStatus} from "../../model/order-status";
import {RowsMapper} from "../../shared/mapper/rows.mapper";


export class MapperOrder implements RowMapper<OrderModel>, RowsMapper<OrderModel> {
  mapRow(from: any, index: number): OrderModel {
    let order: OrderModel = new OrderModel();
    order.id = from.id;
    order.firstName = from.first_name;
    order.lastName = from.last_name;
    order.status = new OrderStatus(from.status);
    order.orderItems = new MapperOrderItem().mapList(from.order_items, index);
    return order;
  }

  mapList(from: any, index: number): OrderModel[] {
    let orders: OrderModel[] = [];
    for (let i = 0; i < from.length; i++) {
      orders.push(this.mapRow(from[i], i));
    }
    return orders;
  }
}
