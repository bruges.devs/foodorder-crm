import {RowMapper} from "../../shared/mapper/rowmapper";
import {BasketItem} from "../../model/basket-item";
import {MapperProduct} from "./mapper.product";
import {RowsMapper} from "../../shared/mapper/rows.mapper";


export class MapperOrderItem implements RowMapper<BasketItem>, RowsMapper<BasketItem> {

  mapRow(from: any, index: number): BasketItem {
    let item = new BasketItem();
    item.amount = from.amount;
    item.price = from.price;
    item.product = new MapperProduct().mapRow(from.product, index);
    return item;
  }

  mapList(from: any, index: number): BasketItem[] {
    let items: BasketItem[] = [];
    for (let i = 0; i < from.length; i++) {
      items.push(this.mapRow(from[i], i));
    }
    return items;
  }
}
