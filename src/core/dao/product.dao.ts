import {Injectable} from "@angular/core";
import {LoggingService} from "../shared/service/logging.service";
import {DatabaseService, DatabaseStatement} from "../shared/service/database.service";
import {Product} from "../../providers";
import {ProductModel} from "../model/product.model";

@Injectable()
export class ProductDao {


  constructor(private logger: LoggingService,
              private databaseService: DatabaseService<Product>) {
  }

  saveOrReplaceProducts(products: ProductModel[]): Promise<boolean> {
    let statements = [];

    for (let i = 0; i < products.length; i++) {
      statements.push(new DatabaseStatement("INSERT OR REPLACE INTO tbl_product (id, category_id, name, description, price) VALUES (?, ?, ?, ?, ?)"
        , products[i].getParams()));
    }

    return this.databaseService.executeBatch(statements);
  }
}
