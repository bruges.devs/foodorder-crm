export class OrderStatus {
  status_in_progress: string = "IN_PROGRESS";
  status_approved: string = "APPROVED";
  status_rejected: string = "REJECTED";

  private _status: string;

  get status(): string {
    return this._status;
  }

  constructor(status: string) {
    if (status !== "") {
      this._status = status;
    } else {
      this._status = this.status_in_progress;
    }
  }

  approve() {
    console.log("Change orderstatus from " + this._status + " to " + this.status_approved);
    this._status = this.status_approved;
  }

  reject() {
    console.log("Change orderstatus from " + this._status + " to " + this.status_rejected);
    this._status = this.status_rejected;
  }

  isInProgress(): boolean {
    return this.status_in_progress === this._status;
  }

  isApproved(): boolean {
    return this.status_approved === this._status;
  }

  isRejected(): boolean {
    return this.status_rejected === this._status;
  }

  statusToString() {
    switch (this._status) {
      case this.status_approved:
        return "Goedgekeurd";
      case this.status_in_progress:
        return "Wachten op goedkeuring";
      case this.status_rejected:
        return "We kunnen de bestelling niet op tijd voorzien";
    }
  }
}
