import {SettingModel} from "./setting.model";
import {SettingTypeEnum} from "../../enum/setting-type.enum";

export class SettingNumber extends SettingModel{
  type: SettingTypeEnum = SettingTypeEnum.NUMBER;
  value: number;

  constructor(value: any) {
    super();
    this.value = +value;
  }

  getValueForApi(): string {
    return this.value.toString();
  }

}
