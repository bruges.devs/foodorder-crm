import {BasketItem} from "./basket-item";
import {OrderStatus} from "./order-status";
import moment = require("moment");

export class OrderModel {

  static TIME_FORMAT: string = "HH:mm";
  static DATE_TIME_FORMAT: string = "DD/MM/YYYY HH:mm";

  private _dateWanted: Date;
  private _status: OrderStatus;

  id: number;
  description: string;
  datePlaced: Date;
  firstName: string;
  lastName: string;
  reason: string;
  orderItems: BasketItem[];

  getFullName(): string {
    return this.firstName + ' ' + this.lastName;
  }

  geStatus(): string {
    return this._status.status;
  }

  set status(value: OrderStatus) {
    this._status = value;
  }

  constructor() {
    this.description = '';
    this.datePlaced = new Date(Date.now());
    this.orderItems = [];
  }

  statusToString(): string {
    return this._status.statusToString();
  }

  get dateWanted(): Date {
    return this._dateWanted;
  }

  set dateWanted(value: Date) {
    if (moment(value, OrderModel.DATE_TIME_FORMAT, true).isValid()) {
      console.debug("Date wanted setted on: ", moment(value, OrderModel.DATE_TIME_FORMAT).toDate());
      this._dateWanted = moment(value, OrderModel.DATE_TIME_FORMAT, true).toDate();
    } else if (moment(value, OrderModel.TIME_FORMAT, true).isValid()) {
      console.debug("Date wanted setted on: ", moment(value, OrderModel.TIME_FORMAT).toDate());
      this._dateWanted = moment(value, OrderModel.TIME_FORMAT).toDate();
    } else {
      console.error("Wrong dateformat given to dateWanted in OrderModel: ", value);
    }
  }

  getTotalPrice(): number {
    let price = 0;
    for (let i = 0; i < this.orderItems.length; i++) {
      price = price + this.orderItems[i].getTotalPrice();
    }
    return price;
  }

  getParams(): any[] {
    return [
      this.description,
      this.getTotalPrice(),
      moment(this.datePlaced).format(OrderModel.DATE_TIME_FORMAT),
      moment(this._dateWanted).format(OrderModel.DATE_TIME_FORMAT),
      this._status.status
    ];
  }

  approve() {
    if (this._status) {
      this._status.approve();
    } else {
      console.error("Status is null when approve");
    }
  }

  reject() {
    if (this._status) {
      this._status.reject();
    } else {
      console.error("Status is null when approve");
    }
  }

  isInProgress(): boolean {
    return this._status.isInProgress();
  }
}
