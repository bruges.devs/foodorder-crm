import {Product} from "../../providers";

export class ProductModel {

  private readonly _product: Product;
  private _isSaved: boolean = true;

  isActive: boolean = false;

  get isSaved(): boolean {
    return this._isSaved;
  }

  get product(): Product {
    return this._product;
  }

  constructor(product: Product) {
    this._product = product;
  }

  toggleProductContainer() {
    this.isActive = !this.isActive;
  }

  productChanged() {
    this._isSaved = false;
  }

  getParams() {
    return [this.product.id, this.product.category ? this.product.category.id : -1, this.product.name, this.product.description ? this.product.description : '', this.product.price];
  }
}
