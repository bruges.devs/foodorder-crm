import {Injectable} from "@angular/core";
import {LoggingService} from "../shared/service/logging.service";
import {Events} from "ionic-angular";
import {EventChannels} from "../constants/event-channels";
import {OrderControllerService} from "../../providers";
import {LogLevel} from "../shared/constants/loglevel";
import {OrderModel} from "../model/order.model";
import {MapperOrder} from "../mapper/api/mapper.order";

@Injectable()
export class OrderService {

  constructor(private logger: LoggingService,
              private orderController: OrderControllerService,
              private events: Events) {
  }

  getAllOrders() {
    //TODO GET ORDERS VAN PARTIJ ID
    this.logger.log(LogLevel.DEBUG, "Load orders", true);
    this.orderController.getOrdersUsingGET().toPromise()
      .then(orders => {
        this.logger.log(LogLevel.DEBUG, "Orders loaded: ", orders);
        return new MapperOrder().mapList(orders, -1)
      })
      .then(orders => this.events.publish(EventChannels.ORDERS_LOADED, orders))
      .catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error while getAllOrders: ", null);
        //TODO SHOW NO PRODUCTS MESSAGE WITH RELOAD POSSIBILITIES
      });
  }

  approveOrder(order: OrderModel) {
    this.orderController.approveOrderUsingPUT(order.id).toPromise()
      .then(() => {
        this.events.publish(EventChannels.ORDER_APPROVED, order);
      }).catch(reason => {
      this.logger.log(LogLevel.ERROR, "Error while approving order in API: ", {reason: reason, order: order});
      //TODO SHOW MESSAGE
    });
  }

  rejectOrder(order: OrderModel) {
    this.orderController.rejectOrderUsingPUT(order.id, order.reason).toPromise().then(() => {
      this.events.publish(EventChannels.ORDER_REJECTED, order);
    }).catch(reason => {
      this.logger.log(LogLevel.ERROR, "Error while rejecting order in API: ", {reason: reason, order: order});
      //TODO SHOW MESSAGE
    });
  }

}
