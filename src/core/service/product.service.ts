import {Injectable} from "@angular/core";
import {Product, ProductControllerService} from "../../providers";
import "rxjs/add/operator/map";
import {ProductDao} from "../dao/product.dao";
import {LoggingService} from "../shared/service/logging.service";
import {LogLevel} from "../shared/constants/loglevel";
import {ProductModel} from "../model/product.model";
import {UpdateProductRequest} from "../../providers/model/updateProductRequest";

@Injectable()
export class ProductService {

  constructor(private productController: ProductControllerService,
              private productDao: ProductDao,
              private logger: LoggingService) {
    this.productController = productController;
  }

  loadProducts(): Promise<Product[]> {
    return this.productController.getProductsUsingGET()
      .map(items => {
        this.logger.log(LogLevel.DEBUG, "Retrieved products from service: ", items);
        return items;
      }).toPromise();
  }

  saveProduct(product: ProductModel): Promise<boolean> {
    let updateRequest: UpdateProductRequest = {
      name: product.product.name,
      description: product.product.description,
      price: product.product.price,
      categoryId: product.product.category.id
    };

    //TODO PRODUCT ID SHOULD BE PRODUCT ID FROM THE BACKEND
    return this.productController.saveProductUsingPUT(product.product.id, updateRequest)
      .toPromise()
      .then((savedProduct: Product) => {
        this.logger.log(LogLevel.FINE, "Response van saveProductUsingPut: ", savedProduct);
        return true;
      })//TODO CHECK ON HTTP RESPONSE CODE
      .catch(reason => {
        this.logger.log(LogLevel.DEBUG, "Kon het product niet naar de backend sturen: ", product);
        return false;
      });
  }

  createProduct(product: Product): Promise<ProductModel> {
    let createProduct = {
      name: product.name,
      description: product.description,
      price: product.price,
      categoryId: product.category ? product.category.id : null
    };

    return this.productController.createProductUsingPOST(createProduct).toPromise()
      .then((savedProduct: Product) => new ProductModel(savedProduct));
  }
}
