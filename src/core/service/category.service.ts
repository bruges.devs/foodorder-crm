import {Injectable} from "@angular/core";
import {LoggingService} from "../shared/service/logging.service";
import {Category, CategoryControllerService} from "../../providers";
import {MapperCategory} from "../mapper/api/mapper.category";
import {CategoryModel} from "../model/category.model";
import {LogLevel} from "../shared/constants/loglevel";
import {CreateCategoryRequest} from "../../providers/model/createCategoryRequest";
import {Events} from "ionic-angular";
import {EventChannels} from "../constants/event-channels";

@Injectable()
export class CategoryService {

  constructor(private logger: LoggingService,
              private categoryController: CategoryControllerService,
              private events: Events) {
  }

  getCategories(){
    this.categoryController.getCategoriesUsingGET().toPromise()
      .then((categories: Category[]) => new MapperCategory().mapList(categories, -1))
      .then((categories: CategoryModel[]) => {
        this.logger.log(LogLevel.DEBUG, "Categories loaded: ", categories);
        this.events.publish(EventChannels.CATEGORIES_LOADED, categories);
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error while loading categories from API: ", reason);
        //TODO SHOW MESSAGE THAT CATEGORIES COULD NOT BE LOADED
    });
  }

  addCategory(name: string): Promise<CategoryModel> {
    let createCategory: CreateCategoryRequest = {name: name};
    return this.categoryController.createCategoryUsingPOST(createCategory)
      .toPromise()
      .then((category: Category) => {
        return new MapperCategory().mapRow(category, 0);
      });
  }
}
