import {Injectable} from "@angular/core";
import {SettingControllerService} from "../../providers/api/settingController.service";
import {Setting} from "../../providers/model/setting";
import {MapperSetting} from "../mapper/api/mapper.setting";
import {SettingModel} from "../model/setting/setting.model";
import {LoggingService} from "../shared/service/logging.service";
import {LogLevel} from "../shared/constants/loglevel";
import {ChangeSettingCommand} from "../../providers/model/changeSettingCommand";

@Injectable()
export class SettingService {

  constructor(private settingsController: SettingControllerService,
              private logger: LoggingService) {
  }

  loadSettings(): Promise<SettingModel[]> {
    return this.settingsController.getSettingsUsingGET().toPromise()
      .then((settings: Setting[]) => {
        let mappedSettings: SettingModel[] = new MapperSetting().mapList(settings);
        this.logger.log(LogLevel.DEBUG, "Settings Loaded from service: ", mappedSettings);
        return mappedSettings;
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Failed to load settings from API: ", reason);
        return [];
      });
  }

  changeSetting(setting: SettingModel): Promise<boolean> {
    const changeSettingCommand: ChangeSettingCommand = {
      id: setting.id,
      key: setting.key,
      value: setting.getValueForApi()
    };

    return this.settingsController.changeSettingUsingPOST(changeSettingCommand)
      .toPromise()
      .then(_ => {
        this.logger.log(LogLevel.DEBUG, "Response from changeSetting to api: ", [setting, _]);
        return true;
      })
      .catch(reason => {
        this.logger.log(LogLevel.ERROR, "Failed to update setting: ", reason);
        return false;
      })
  }
}
