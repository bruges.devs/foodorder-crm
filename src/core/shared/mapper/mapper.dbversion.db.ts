import {RowMapper} from "./rowmapper";

export class DatabaseVersionMapper implements RowMapper<DbVersion> {

  constructor() { }

  mapRow(from: any, index: number): DbVersion {
    if (from == undefined || from == null) {
      return undefined;
    } else {
      let row = from.item(index);

      let dbVersion = new DbVersion(
        row.dbVersionId,
        row.version,
        row.type,
        row.modifyDate
      );

      return dbVersion;
    }
  }

}

export class DbVersion {

  private _dbVersionId: string;
  private _version: string;
  private _type: string;
  private _modifyDate: Date;


  constructor(dbVersionId: string, version: string, type: string, modifyDate: Date) {
    this._dbVersionId = dbVersionId;
    this._version = version;
    this._type = type;
    this._modifyDate = modifyDate;
  }


  get dbVersionId(): string {
    return this._dbVersionId;
  }

  set dbVersionId(value: string) {
    this._dbVersionId = value;
  }

  get version(): string {
    return this._version;
  }

  set version(value: string) {
    this._version = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get modifyDate(): Date {
    return this._modifyDate;
  }

  set modifyDate(value: Date) {
    this._modifyDate = value;
  }
}

