import {Injectable} from "@angular/core";
import {LoggingService} from "./logging.service";
import {DbSchema} from "../../constants/db-schema";
import {LogLevel} from "../constants/loglevel";
import {RowMapper} from "../mapper/rowmapper";
import {DatabaseVersionMapper, DbVersion} from "../mapper/mapper.dbversion.db";
import {DbQueries} from "../../constants/db-queries";

export interface Window {
  openDatabase: any;
}

declare var window: Window;


@Injectable()
export class DatabaseService<T> {

  private _db: any;


  constructor(private logger: LoggingService) {
    this.initializeDb();
  }

  getDBInstanceSync(): any {
    return this._db;
  }

  createDatabaseDuringFirstStartup(): Promise<boolean> {
    this.logger.log(LogLevel.INFO, 'isFirstStartup, creating database', undefined);
    return this.createDatabase();
  };

  executeSql(statement: DatabaseStatement): Promise<boolean> {
    let that = this;

    return new Promise((resolve, reject) => {
      return this.getDBInstanceSync().transaction(function (t) {
        t.executeSql(statement.statement, statement.params, (tx, rs) => {
            resolve(true);
          },
          (t, err) => {
            that.logger.log(LogLevel.ERROR, "Database error while executing executeSql", [err, statement]);
            reject(err);
          }
        );
      }, function () {
        return true;
      });
    });
  }

  getSingleResult(statement: DatabaseStatement, mapper: RowMapper<T>): Promise<T> {
    let that = this;

    return new Promise((resolve, reject) => {
      return this.getDBInstanceSync().transaction(function (t) {
        t.executeSql(statement.statement, statement.params, (tx, rs) => {
            if (rs.rows!.length! > 0 && mapper !== undefined) {
              resolve(mapper.mapRow(rs.rows, 0));
            } else {
              resolve(null);
            }
          },
          (t, err) => {
            that.logger.log(LogLevel.ERROR, "Database error while executing getSingleResult", [err, statement]);
            reject(err);
          }
        );
      }, function () {
        return true;
      });
    });
  }

  getResultList(statement: DatabaseStatement, mapper: RowMapper<T>): Promise<Array<T>> {
    let that = this;

    return new Promise((resolve, reject) => {
      return this.getDBInstanceSync().transaction(function (t) {
        t.executeSql(statement.statement, statement.params, (tx, rs) => {
            if (rs.rows!.length! > 0) {
              let result: Array<T> = [];
              for (let i = 0; i < rs.rows.length; i++) {
                result.push(mapper.mapRow(rs.rows, i));
              }
              resolve(result);
            } else {
              resolve([]);
            }
          },
          (t, err) => {
            that.logger.log(LogLevel.ERROR, "Database error while executing getResultList", [err, statement]);
            reject(err);
          }
        );
      }, function () {
        return true;
      });
    });
  }

  insertEntity(entity: T, statement: DatabaseStatement, idPropertyName: string): Promise<T> {
    let that = this;

    return new Promise((resolve, reject) => {
      return this.getDBInstanceSync().transaction(function (t) {
        t.executeSql(statement.statement, statement.params, (tx, rs) => {
            entity[idPropertyName] = rs.insertId;
            resolve(entity);
          },
          (t, err) => {
            that.logger.log(LogLevel.ERROR, "Database error while executing insertEntity", [err, statement]);
            reject(err);
          }
        );
      }, function () {
        return true;
      });
    });
  }

  executeBatch(statements: Array<DatabaseStatement>): Promise<boolean> {
    this.logger.log(LogLevel.FINE, "Executing sql batch", statements);

    if (statements.length == 0) {
      let err = new Error();
      this.logger.log(LogLevel.FINE, "Empty execute batch scripts passed", err.stack);
      return new Promise((resolve, reject) => resolve(true));
    }

    let that = this;

    return new Promise((resolve, reject) => {
      return this.getDBInstanceSync().transaction(function (t) {
        let count = statements.length;
        for (let i = 0; i < statements.length; i++) {
          t.executeSql(statements[i].statement, statements[i].params, (tx, rs) => {
              if (count = i - 1) {
                resolve(true);
              }
            },
            (t, err) => {
              that.logger.log(LogLevel.ERROR, "Database error while executing executeBatch",
                [err, statements[i].statement, JSON.stringify(statements[i].params)]);
              reject(err);
            });
        }
      }, function () {
        resolve(true);
      });
    });
  }

  getDatabaseVersion(): Promise<DbVersion> {
    let that = this;
    let mapper = new DatabaseVersionMapper();

    return new Promise((resolve, reject) => {
      this.getDBInstanceSync().transaction((t) => {
        t.executeSql(DbQueries.GET_DB_VERSION, [], (tx, rs) => {
            if (rs.rows != undefined && rs.rows != null && rs.rows.length > 0) {
              let dbVersion: DbVersion = mapper.mapRow(rs.rows, 0);
              resolve(dbVersion);
            } else {
              resolve(null);
            }
          },
          (t, err) => {
            that.logger.log(LogLevel.ERROR, "Database error while executing getDatabaseVersion", [err, DbQueries.GET_DB_VERSION, []]);
            reject(err);
          }
        );
      }, function () {
        return true;
      });
    });
  }

  private initializeDb(): void {
    if (this._db == null) {
      this._db = window.openDatabase('foodorder.db', '0.0.1', 'Food order Database', 2500000);
      this.logger.log(LogLevel.DEBUG, "Database created and opened", this._db);
    }
  }

  private createDatabase(): Promise<boolean> {
    return this.executeBatch([
      new DatabaseStatement(DbSchema.CREATE_TABLE_DB_VERSION, []),
      new DatabaseStatement(DbSchema.CREATE_TABLE_PRODUCT, []),
      new DatabaseStatement(DbQueries.INSERT_INITIAL_DB_VERSION, ['1.0', 'INITIAL_CREATE', Date.now()])
    ]);
  }
}

export class DatabaseStatement {

  private _statement: string;
  private _params: Array<any>;

  constructor(statement: string, params: Array<any>) {
    this._statement = statement;
    this._params = params;
  }

  get statement(): string {
    return this._statement;
  }

  get params(): Array<any> {
    return this._params;
  }

}
