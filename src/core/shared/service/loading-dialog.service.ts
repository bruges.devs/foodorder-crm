import {Injectable} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {LoggingService} from "./logging.service";
import {Events, Loading, LoadingController} from "ionic-angular";
import {EventChannels} from "../../constants/event-channels";
import {LogLevel} from "../constants/loglevel";

export const LOAD_PRODUCTS: string = 'LOAD_PRODUCTS';

@Injectable()
export class LoadingDialogService {


  private loadingDialogs = [];
  private loading: Loading;

  constructor(private events: Events,
              private translateService: TranslateService,
              private loadingController: LoadingController,
              private logger: LoggingService) {

    this.events.subscribe(EventChannels.CHANNEL_BACKEND_NOT_AVAILABLE, (result) => {
      if (result) {
        this.dismissAllLoadingDialogs();
      }
    });

  }

  public showLoadingDialog(messageKey: string) {
    this.logger.log(LogLevel.INFO, 'showLoadingDialog - ', messageKey);

    if(this.loadingDialogs.indexOf(messageKey) === -1){
      this.loadingDialogs.push(messageKey);
    }

    if (!this.loading) {
      this.loading = this.loadingController.create({
        content: this.translateService.instant(messageKey)
      });
    } else {
      this.loading.setContent(this.translateService.instant(messageKey));
    }
    this.loading.present();
  }

  public dismissLoadingDialog(messageKey: string): void {
    this.logger.log(LogLevel.INFO, 'dismissLoadingDialog - ', messageKey);
    this.loadingDialogs.splice(this.loadingDialogs.indexOf(messageKey),1 );

    if (this.loadingDialogs.length > 0) {
      this.logger.log(LogLevel.INFO, "dismissLoadingDialog and put other text on it", undefined);
      let nextKey = this.loadingDialogs[this.loadingDialogs.length-1];
      this.loading.setContent(this.translateService.instant(nextKey));
    } else{
      this.logger.log(LogLevel.INFO, "dismissLoadingDialog last action dismiss", undefined);
      if (this.loading) {
        try {
          this.loading.dismiss();
          this.loading = undefined;
        } catch (e) {
        }
      }
    }
  }

  public dismissAllLoadingDialogs(): void {
    this.loadingDialogs.forEach((loadingDialog: string) => {
      this.dismissLoadingDialog(loadingDialog);
    });
  }
}
