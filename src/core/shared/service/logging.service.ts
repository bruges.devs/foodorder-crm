import {Injectable} from "@angular/core";
import {LogLevel} from "../constants/loglevel";
import {Config} from "../../../config/config";

declare var window: any;


@Injectable()
export class LoggingService {

  private levels: Array<LogLevel> = Config.LOG_LEVELS;

  private logger = window.logToFile;

  private consoleEnabled: boolean = true;
  private logfileEnabled: boolean = true;

  constructor() {
    let t = this;
    if (this.logger){
      console.log("LoggingService constructor triggered ...");
      let logFile = 'digibb.log';
      this.logger.setLogfilePath(logFile, function () {
        console.log("Log file was created ...");
        t.logger.getArchivedLogfilePaths(function (archivedLogFiles) {
          console.log("Archived log paths", archivedLogFiles);
        }, function (err) {
          console.error("Archived log paths retieval failed", err);
        });
      }, function (err) {
        // logfile could not be written
        console.error("file was not created");
      });
    } else {
      this.logfileEnabled = false;
    }
  }

  public log(logLevel: LogLevel, message: string, data: any) {
    if (this.consoleEnabled && this.levels.indexOf(logLevel) != -1) {
      let concatenatedMessage = new Date() + " : " + LogLevel[logLevel] + ' : ' + message;

      switch(logLevel) {
        case LogLevel.FINE:
          if (data !== undefined) {
            console.log(concatenatedMessage, data);
          } else {
            console.log(concatenatedMessage);
          }
          break;
        case LogLevel.DEBUG:
          if (data !== undefined) {
            console.log(concatenatedMessage, data);
          } else {
            console.log(concatenatedMessage);
          }
          break;
        case LogLevel.INFO:
          if (data !== undefined) {
            console.info(concatenatedMessage, data);
          } else {
            console.info(concatenatedMessage);
          }
          break;
        case LogLevel.WARN:
          if (data !== undefined) {
            console.warn(concatenatedMessage, data);
          } else {
            console.warn(concatenatedMessage);
          }
          break;
        case LogLevel.ERROR:
          if (data !== undefined) {
            console.error(concatenatedMessage, data);
          } else {
            console.error(concatenatedMessage);
          }
          break;
        default:
          if (data !== undefined) {
            console.log(concatenatedMessage, data);
          } else {
            console.log(concatenatedMessage);
          }
      }
    }

    if (this.logfileEnabled && this.levels.indexOf(logLevel) != -1) {
      let tmpMessage = message;
      if (data != undefined && data != null) {
        try {
          tmpMessage += " : " + this.stringify(data) + "\r\n";
        } catch (e) {
          this.log(LogLevel.ERROR, "Could not stringify the data", undefined);
        }
      }

      switch(logLevel) {
        case LogLevel.FINE:
          this.logger.debug(tmpMessage + "\r\n");
          break;
        case LogLevel.DEBUG:
          this.logger.debug(tmpMessage + "\r\n");
          break;
        case LogLevel.INFO:
          this.logger.info(tmpMessage + "\r\n");
          break;
        case LogLevel.WARN:
          this.logger.warn(tmpMessage + "\r\n");
          break;
        case LogLevel.ERROR:
          this.logger.error(tmpMessage + "\r\n");
          break;
      }
    }
  }

  private stringify(data) {
    let cache = [];
    let result = JSON.stringify(data, function(key, value) {
      if (typeof value === 'object' && value !== null) {
        if (cache.indexOf(value) !== -1) {
          // Circular reference found, discard key
          return;
        }
        // Store value in our collection
        cache.push(value);
      }
      return value;
    });
    cache = null; // Enable garbage collection
    return result;
  }

}
