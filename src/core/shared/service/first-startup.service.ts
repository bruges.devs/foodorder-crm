import {Injectable} from "@angular/core";
import {DatabaseService} from "./database.service";
import {LoggingService} from "./logging.service";
import {LogLevel} from "../constants/loglevel";
import {DbVersion} from "../mapper/mapper.dbversion.db";


@Injectable()
export class FirstStartupService {

  constructor(private databaseService: DatabaseService<DbVersion>,
              private logger: LoggingService) {
  }

  public isFirstStartup(): Promise<boolean> {
    let that = this;

    this.logger.log(LogLevel.INFO, "Check first startup", undefined);

    return new Promise((resolve, reject) => {
      return this.databaseService.getDatabaseVersion().then(function (dbVersion) {
        that.logger.log(LogLevel.INFO, "Current db version", dbVersion);
        resolve(dbVersion == null);
      }, function (error) {
        that.logger.log(LogLevel.WARN, "isFirstStartup failed", error);
        resolve(true);
      });
    });
  }

}
