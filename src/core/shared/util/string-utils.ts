export class StringUtils {

  static join(list: string[]): string{
    let idList = '';
    for (let i = 0; i < list.length; i++) {
      idList = idList + list[i];
      if (i != list.length - 1) {
        idList = idList + ',';
      }
    }
    return idList;
  }
}
