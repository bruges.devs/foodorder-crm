export class EventChannels {

  //GENERAL
  static NETWORK_CHANNEL = "network:channel";
  static CHANNEL_BACKEND_NOT_AVAILABLE = 'backend:not-available';

  //PRODUCTS
  static PRODUCTS_LOADED = 'PRODUCTS_LOADED';
  static PRODUCTS_LOADED_FAILED = 'PRODUCTS_LOADED_FAILED';

  //ORDERS
  static ORDERS_LOADED = 'orders:loaded';
  static ORDER_APPROVED = 'orders:approved';
  static ORDER_REJECTED = 'orders:rejected';

  //CATEGORIES
  static CATEGORY_CATEGORY_ADDED= 'category:added';
  static CATEGORIES_LOADED = 'categories:loaded';

  //SETTINGS
  static SETTING_CAN_APPROVE_UPDATED= 'SETTING_CAN_APPROVE_UPDATED';
}
