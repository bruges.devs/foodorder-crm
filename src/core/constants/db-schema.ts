export class DbSchema {

  public static CREATE_TABLE_DB_VERSION: string = `CREATE TABLE \`db_version\` (
        \`dbVersionId\`	INTEGER NOT NULL,
        \`version\`	TEXT NOT NULL,
        \`type\`	TEXT NOT NULL,
        \`modifyDate\`	INTEGER NOT NULL,
        PRIMARY KEY(dbVersionId)
    );`;

  public static CREATE_TABLE_PRODUCT: string = `CREATE TABLE \`tbl_product\` (
        \`id\`	INTEGER NOT NULL,
        \`category_id\` TEXT NOT NULL,
        \`description\` TEXT NOT NULL,
        \`name\` TEXT NOT NULL,
        \`price\` TEXT NOT NULL,
        PRIMARY KEY(id)
    );`;
}
