import {Component} from '@angular/core';

import {OrderPage} from '../orders/orders';
import {Products} from "../../components/products/products";
import {CategoriesPage} from "../categories/categories";
import {SettingsPage} from "../settings/settings";

@Component({
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = Products;
  tab2Root = OrderPage;
  tab4Root = CategoriesPage;
  tab3Root = SettingsPage;

  constructor() {

  }
}
