import {Component, OnInit} from '@angular/core';
import {AlertController, Events, Refresher} from 'ionic-angular';
import {OrderFacade} from "../facade/order.facade";
import {OrderModel} from "../../core/model/order.model";
import {EventChannels} from "../../core/constants/event-channels";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {LoggingService} from "../../core/shared/service/logging.service";
import {SettingModel} from "../../core/model/setting/setting.model";
import {SettingFacade} from "../facade/setting.facade";

@Component({
  selector: 'orders',
  templateUrl: 'orders.html'
})
export class OrderPage implements OnInit {

  refresher: Refresher;
  can_approve: boolean;

  constructor(protected facade: OrderFacade,
              protected settingFacade: SettingFacade,
              private alertCtrl: AlertController,
              private logger: LoggingService,
              private events: Events) {
    this.events.subscribe(EventChannels.ORDERS_LOADED, () => this.refreshEnded());
  }

  ngOnInit(): void {
    this.events.subscribe(EventChannels.SETTING_CAN_APPROVE_UPDATED, (canApprove: boolean)=>{
      this.can_approve = canApprove;
    });
    this.facade.loadOrders();
    this.can_approve = localStorage.getItem('CAN_APPROVE')==='1';
  }

  doRefresh(refresher: Refresher) {
    this.refresher = refresher;
    this.facade.loadOrders();
  }

  private refreshEnded() {
    if (this.refresher) {
      this.refresher.complete();
    }
  }

  approveOrder(order: OrderModel) {
    this.facade.approveOrder(order);
  }

  rejectOrder(order: OrderModel) {
    let alert = this.alertCtrl.create({
      title: 'Weigeren van bestelling',
      inputs: [
        {
          name: 'reason',
          placeholder: 'Reden'
        }
      ],
      buttons: [
        {
          text: 'Annuleren',
          role: 'cancel',
          handler: data => {
            this.logger.log(LogLevel.DEBUG, "Niet weigeren", order);
          }
        },
        {
          text: 'Bestelling weigeren',
          handler: data => {
            order.reason = data.reason;
            this.facade.rejectOrder(order);
          }
        }
      ]
    });
    alert.present();
  }
}
