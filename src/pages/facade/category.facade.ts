import {Injectable} from "@angular/core";
import {LoggingService} from "../../core/shared/service/logging.service";
import {Events} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {CategoryModel} from "../../core/model/category.model";
import {CategoryService} from "../../core/service/category.service";
import {LogLevel} from "../../core/shared/constants/loglevel";

@Injectable()
export class CategoryFacade {

  categories: CategoryModel[] = [];

  constructor(private logger: LoggingService,
              private events: Events,
              private categoryService: CategoryService) {
    this.events.subscribe(EventChannels.CATEGORY_CATEGORY_ADDED,
      (newCategories: CategoryModel[]) => this.categories = newCategories);

    this.events.subscribe(EventChannels.CATEGORIES_LOADED,
      (loadedCategories: CategoryModel[]) => this.categories = loadedCategories);
  }

  addCategory(name: string) {
    return this.categoryService.addCategory(name)
      .then((addedCategory: CategoryModel) => {
        this.logger.log(LogLevel.DEBUG, "Added category:", addedCategory);
        this.categories.push(addedCategory);
        this.events.publish(EventChannels.CATEGORY_CATEGORY_ADDED, this.categories);
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error while loading categories from API: ", reason);
        //TODO SHOW MESSAGE THAT CATEGORIES COULD NOT BE LOADED
      });

  }

  getCategories() {
    this.categoryService.getCategories();
  }
}
