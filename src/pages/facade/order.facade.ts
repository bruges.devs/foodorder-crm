import {Injectable} from "@angular/core";
import {OrderModel} from "../../core/model/order.model";
import {Events} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {OrderService} from "../../core/service/order.service";
import {LoggingService} from "../../core/shared/service/logging.service";
import {LogLevel} from "../../core/shared/constants/loglevel";

@Injectable()
export class OrderFacade {

  private orders: OrderModel[] = [];

  constructor(private events: Events,
              private logger: LoggingService,
              private orderService: OrderService) {
    this.events.subscribe(EventChannels.ORDERS_LOADED, (orders: OrderModel[]) => {
      this.logger.log(LogLevel.DEBUG, "Orders are loaded: ", orders);
      this.orders = orders;
    });

    this.events.subscribe(EventChannels.ORDER_APPROVED, (order: OrderModel) => {
      this.changeOrderStatus(true, order);
    });

    this.events.subscribe(EventChannels.ORDER_REJECTED, (order: OrderModel) => {
      this.changeOrderStatus(false, order);
    });
  }

  loadOrders() {
    this.orderService.getAllOrders();
  }

  approveOrder(order: OrderModel) {
    this.orderService.approveOrder(order);
  }

  rejectOrder(order: OrderModel){
    this.orderService.rejectOrder(order);
  }

  private changeOrderStatus(isApproved: boolean, order: OrderModel) {
    let filteredOrders: OrderModel[] = this.orders.filter(value => value.id === order.id);
    if (filteredOrders.length === 1) {
      if (isApproved) {
        filteredOrders[0].approve();
      } else {
        filteredOrders[0].reject();
      }
    } else {
      this.logger.log(LogLevel.ERROR, "Het order werd niet gevonden", [this.orders, order]);
    }
  }

}
