import {Injectable} from "@angular/core";
import {SettingModel} from "../../core/model/setting/setting.model";
import {SettingService} from "../../core/service/setting.service";
import {Events} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";

@Injectable()
export class SettingFacade {

  settings: SettingModel[] = [];

  constructor(private settingService: SettingService,
              private events: Events) {
  }

  loadSettings() {
    this.settingService.loadSettings()
      .then(loadedSettings => this.settings = loadedSettings)
      .then(() => {
        const canApproveSetting = this.loadSettingByKey('CAN_APPROVE');
        if (canApproveSetting) {
          localStorage.setItem('CAN_APPROVE', canApproveSetting.value);
          return canApproveSetting.value==='1';
        }
        return true;
      })
      .then((canApprove:boolean)=>this.events.publish(EventChannels.SETTING_CAN_APPROVE_UPDATED, canApprove));
  }

  loadSettingByKey(key: string): SettingModel {
    return this.settings.find(x => x.key === key);
  }

  changeSetting(setting: SettingModel) {
    this.settingService.changeSetting(setting)
      .then((hasUpdated: boolean) => {
        //TODO SET PREVIOUS SETTING VALUE IF FAILED
        this.loadSettings();
      });
  }
}
