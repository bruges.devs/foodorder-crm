import {Injectable} from "@angular/core";
import {ProductService} from "../../core/service/product.service";
import {Product} from "../../providers";
import {LogLevel} from "../../core/shared/constants/loglevel";
import {LoggingService} from "../../core/shared/service/logging.service";
import {EventChannels} from "../../core/constants/event-channels";
import {NetworkService} from "../../core/shared/service/network.service";
import {MyToastService} from "../../core/shared/service/my-toast.service";
import {ProductModel} from "../../core/model/product.model";
import {Events} from "ionic-angular";
import {Config} from "../../config/config";
import {CategoryModel} from "../../core/model/category.model";
import {CategoryService} from "../../core/service/category.service";

@Injectable()
export class ProductFacade {

  products: ProductModel[] = [];
  categories: CategoryModel[] = [];

  constructor(private productService: ProductService,
              private categoryService: CategoryService,
              private events: Events,
              private toastCtrl: MyToastService,
              private network: NetworkService,
              private logger: LoggingService) {

    this.events.subscribe(EventChannels.CATEGORY_CATEGORY_ADDED,
      (newCategories: CategoryModel[]) => this.categories = newCategories);

    this.events.subscribe(EventChannels.CATEGORIES_LOADED,
      (loadedCategories: CategoryModel[]) => this.categories = loadedCategories);
  }

  initialProductsLoad(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      return this.productService.loadProducts().then(loadedProducts => {
        //TRANSFORM
        this.products = [];
        loadedProducts.forEach(product => {
          this.products.push(new ProductModel(product));
        });
        resolve(true);
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error in initialProductsLoad()", reason);
        this.events.publish(EventChannels.PRODUCTS_LOADED_FAILED);
        this.toastCtrl.showToast("De producten konden niet opgehaald worden.", Config.TOAST_DURATION_NORMAL);
      });
    });
  }

  loadCategories() {
    if (this.network.hasConnection()) {
      this.categoryService.getCategories();
    }
  }

  loadProducts() {
    if (this.network.hasConnection()) {
      this.productService.loadProducts().then(loadedProducts => {
        //TRANSFORM
        this.products = [];
        loadedProducts.forEach(product => {
          this.products.push(new ProductModel(product));
        });

        //this.saveProducts(this.products);
        this.events.publish(EventChannels.PRODUCTS_LOADED);
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Error while loading products", reason);
        this.events.publish(EventChannels.PRODUCTS_LOADED_FAILED);
      });
    }
  }

  createProduct(product: Product) {
    return this.productService.createProduct(product)
      .then((savedProduct: ProductModel) => {
        this.products.push(savedProduct);
        this.toastCtrl.showToast(savedProduct.product.name + " werd toegevoegd aan de productenlijst",
          Config.TOAST_DURATION_NORMAL);
      }).catch(reason => {
        this.logger.log(LogLevel.ERROR, "Het product kon niet opgeslagen worden: ", product);
        this.toastCtrl.showErrorToast("Het product kon niet opgeslagen worden", Config.TOAST_DURATION_NORMAL);
      });
  }

  saveProduct(product: ProductModel) {
    this.productService.saveProduct(product)
      .then((isSaved: boolean) => {
        if (isSaved) {
          this.toastCtrl.showToast(product.product.name + " opgeslagen", Config.TOAST_DURATION_NORMAL);
        } else {
          this.toastCtrl.showErrorToast("Product kon niet opgeslagen worden", Config.TOAST_DURATION_NORMAL);
        }
      });
  }

  pickCategory(categoryId: string): CategoryModel {
    this.logger.log(LogLevel.DEBUG, "Gekozen category: ", categoryId);
    return this.categories.filter(value => value.id === categoryId)[0];
  }
}
