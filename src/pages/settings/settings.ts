import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {OpeningHoursManagementPage} from "../opening-hours-management/opening-hours-management";
import {SettingFacade} from "../facade/setting.facade";
import {SettingModel} from "../../core/model/setting/setting.model";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private facade: SettingFacade) {
  }

  shopShopHours() {
    this.navCtrl.push(OpeningHoursManagementPage);
  }

  changeSetting($event: any, setting: SettingModel) {
    setting.value = $event.value;
   this.facade.changeSetting(setting);
  }
}
