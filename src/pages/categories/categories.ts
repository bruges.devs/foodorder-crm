import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CategoryFacade} from "../facade/category.facade";

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage implements OnInit {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private facade: CategoryFacade) {
  }

  ngOnInit(): void {
    this.facade.getCategories();
  }

  createNewCategory() {
    let alert = this.alertCtrl.create({
      title: 'Categorie aanmaken',
      inputs: [
        {
          name: 'name',
          placeholder: 'Categorie naam'
        }
      ],
      buttons: [
        {
          text: 'Annuleren',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Toevoegen',
          handler: data => {
            this.facade.addCategory(data.name);
          }
        }
      ]
    });

    alert.present();
  }

}
