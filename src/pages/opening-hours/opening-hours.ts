import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {OpeningHoursFacade} from "../facade/opening-hours.facade";

/**
 * Generated class for the OpeningHoursPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-opening-hours',
  templateUrl: 'opening-hours.html'
})
export class OpeningHoursPage {

  constructor(private facade: OpeningHoursFacade) {
  }
}
