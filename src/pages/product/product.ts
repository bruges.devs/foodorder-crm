import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'product.html'
})
export class ProductPage implements OnInit, OnDestroy{

  constructor(public navCtrl: NavController) {

  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }

}
