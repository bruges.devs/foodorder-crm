import {Component} from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {TabsPage} from '../pages/tabs/tabs';
import {LogLevel} from "../core/shared/constants/loglevel";
import {DatabaseService} from "../core/shared/service/database.service";
import {LoggingService} from "../core/shared/service/logging.service";
import {TranslateService} from "@ngx-translate/core";
import {DatabaseUpdatesService} from "../core/shared/service/database-updates.service";
import {FirstStartupService} from "../core/shared/service/first-startup.service";
import {NetworkService} from "../core/shared/service/network.service";
import {AppCreator} from "../core/shared/app/app-creator";
import {AppCreatorListener} from "../core/shared/app/app-creator-listener";
import {ProductFacade} from "../pages/facade/product.facade";
import {SettingFacade} from "../pages/facade/setting.facade";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements AppCreatorListener {

  rootPage: any = TabsPage;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private translate: TranslateService,
              private databaseUpdatesService: DatabaseUpdatesService,
              private firstStartupService: FirstStartupService,
              private logger: LoggingService,
              private databaseService: DatabaseService<any>,
              private networkService: NetworkService,
              private productFacade: ProductFacade,
              private settingsFacade:SettingFacade,
              private appCreator: AppCreator) {
    this.initTranslate();

    this.appCreator.appCreatorListener = this;
    platform.ready().then(() => {
      this.appCreator.prepareApp().then(value => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        statusBar.styleDefault();
        splashScreen.hide();
      });
    });

  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('nl');
    this.translate.use('nl'); // Set your language here
  }

  initializeAppAfterErrors() {
    //TODO
  }

  appIsPreparedHandler(): Promise<boolean> {
    this.logger.log(LogLevel.DEBUG, "Initial products load: ", true);
    this.productFacade.loadCategories();
    this.settingsFacade.loadSettings();
    return this.productFacade.initialProductsLoad();
  }
}
