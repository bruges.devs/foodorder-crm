import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';

import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {SharedModule} from "../core/shared/shared-module";
import {ProductService} from "../core/service/product.service";
import {ProductFacade} from "../pages/facade/product.facade";
import {ProductDao} from "../core/dao/product.dao";
import {ProductPage} from "../pages/product/product";
import {HttpClientModule} from "@angular/common/http";
import {I18nModule} from "../core/shared/i18n/i18n.module";
import {ApiModule} from "../providers";
import {Products} from "../components/products/products";
import {CreateProductDialog} from "../components/create-product-dialog/create-product-dialog";
import {OrderService} from "../core/service/order.service";
import {CategoryService} from "../core/service/category.service";
import {OrderFacade} from "../pages/facade/order.facade";
import {OrderPage} from "../pages/orders/orders";
import {CategoriesPage} from "../pages/categories/categories";
import {CategoryFacade} from "../pages/facade/category.facade";
import {OpeningHoursPage} from "../pages/opening-hours/opening-hours";
import {OpeningHoursFacade} from "../pages/facade/opening-hours.facade";
import {OpeningHoursManagementPage} from "../pages/opening-hours-management/opening-hours-management";
import {SettingsPageModule} from "../pages/settings/settings.module";
import {SettingFacade} from "../pages/facade/setting.facade";
import {SettingService} from "../core/service/setting.service";

@NgModule({
  declarations: [
    MyApp,
    ProductPage,
    OrderPage,
    TabsPage,
    Products,
    CreateProductDialog,
    CategoriesPage,
    OpeningHoursPage,
    OpeningHoursManagementPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    I18nModule,
    ApiModule,
    SharedModule,
    SettingsPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Products,
    ProductPage,
    OrderPage,
    CategoriesPage,
    TabsPage,
    CreateProductDialog,
    OpeningHoursPage,
    OpeningHoursManagementPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ProductDao,
    ProductFacade,
    ProductService,
    OrderService,
    OrderFacade,
    CategoryFacade,
    CategoryService,
    OpeningHoursFacade,
    SettingFacade,
    SettingService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
