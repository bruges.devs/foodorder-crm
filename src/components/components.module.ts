import {NgModule} from '@angular/core';
import {Products} from "./products/products";

@NgModule({
	declarations: [Products],
	imports: [],
	exports: [Products]
})
export class ComponentsModule {}
