import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ProductFacade} from "../../pages/facade/product.facade";
import {AlertController, Events, ModalController, Refresher} from "ionic-angular";
import {EventChannels} from "../../core/constants/event-channels";
import {ProductModel} from "../../core/model/product.model";
import {CreateProductDialog} from "../create-product-dialog/create-product-dialog";
import {CategoryModel} from "../../core/model/category.model";

/**
 * Generated class for the Products component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'products',
  templateUrl: 'products.html'
})
export class Products implements OnInit, OnDestroy {

  private refresher: Refresher;

  constructor(protected facade: ProductFacade,
              private events: Events,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private changeRef: ChangeDetectorRef) {
    this.events.subscribe(EventChannels.PRODUCTS_LOADED, () => this.refreshEnded());
    this.events.subscribe(EventChannels.PRODUCTS_LOADED_FAILED, () => this.refreshEnded());
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }

  doRefresh(refresher: Refresher) {
    this.refresher = refresher;
    this.facade.loadProducts();
    this.facade.loadCategories();
  }

  toggleProductContainer(product: ProductModel) {
    product.toggleProductContainer();
    this.changeRef.detectChanges();
  }

  private refreshEnded() {
    if (this.refresher) {
      this.refresher.complete();
    }
  }

  displayIcon(productModel: ProductModel): string {
    return productModel.isActive ? 'arrow-dropup' : 'arrow-dropdown';
  }

  productChanged(productModel: ProductModel) {
    productModel.productChanged();
  }

  saveProduct(product: ProductModel) {
    this.facade.saveProduct(product);
  }

  createNewProduct() {
    let createProductModal = this.modalCtrl.create(CreateProductDialog, {});
    createProductModal.present();
  }

  showCategoryPicker(product: ProductModel) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Kies categorie');

    this.facade.categories.forEach((category: CategoryModel) => {
      alert.addInput({
        type: 'radio',
        label: category.name,
        value: category.id,
        checked: false
      });
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        product.product.category = this.facade.pickCategory(data);
      }
    });
    alert.present();
  }
}
