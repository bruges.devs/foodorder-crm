import {Component} from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";
import {LoggingService} from "../../core/shared/service/logging.service";
import {MyToastService} from "../../core/shared/service/my-toast.service";
import {Product} from "../../providers";
import {ProductFacade} from "../../pages/facade/product.facade";


/**
 * Generated class for the OrderDialogComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'create-product-dialog',
  templateUrl: 'create-product-dialog.html'
})
export class CreateProductDialog {

  product: Product;

  constructor(private view: ViewController,
              private logger: LoggingService,
              private navParams: NavParams,
              private facade: ProductFacade,
              private toastCtrl: MyToastService) {
    this.product = {
      category: null,
      description: "",
      name: "",
      price: 0
    };
  }

  closeModal() {
    this.view.dismiss();
  }

  saveProduct() {
    this.facade.createProduct(this.product);
    this.view.dismiss();
  }
}
